jQuery(document).ready(function(){
//
// list stylde + clear list styled
// 
// if($('.wysiwyg').length) {
//   $('.wysiwyg').find('ul').addClass('list-styled');
//   $('.wysiwyg').find('ul').find('ul').removeClass('list-styled');

//   if($('ul.list-styled').find('li > ul').length > 0) {
//     var $ul_second_level = $('ul.list-styled').find('li > ul'),
//         $li = $ul_second_level.closest('li');

//     $li.addClass('clear-list-style');
//   };
// };

// 
// add popup animation + classes for animate
// 
if($('.open-popup-link').length) {
  $('.open-popup-link').attr('data-effect', 'mfp-zoom-in'); 
}
if($('.popup').length) {
  if(!$('.popup').hasClass('mfp-with-anim mfp-hide')) {
    $('.popup').addClass('mfp-with-anim mfp-hide'); 
  }
}
// 
// main header and mobile nav
// 
$('.menu-trigger').on('click', function(){
  $(this).toggleClass('active');
  $('.main-header__nav').toggleClass('active');
    // scroll disabled
    if($(this).hasClass('active')) {
      if(!device.android()) {
        // document.ontouchmove = function(e){ e.preventDefault()};
      } else {
        $('html').css('overflow', 'hidden');
      }
    } else {
      if(!device.android()) {
        // document.ontouchmove = function(e){ e.stopPropagation()};
      } else {
        $('html').css('overflow', 'auto');
      }
    }
});

// thin nav
$(window).on('scroll', function(){
  var $scroll = $(this).scrollTop();

  if ($scroll > 100) {
    $('.main-header').addClass('main-header--thin');
  } else {
    $('.main-header').removeClass('main-header--thin');
    $('.main-header__action-link').addClass('temporary');
    setTimeout(function(){
      $('.main-header__action-link').removeClass('temporary');
    }, 300);
  }
});

// 
// dropdowns
// 
$('.main-header__action-link').on('click', function(){
  var $this_link = $(this),
      $this_dropdown = $this_link.siblings('.main-header__dropdown'),
      // mobile
      $window_width = $(window).width(),
      $target_link = $this_link.data('dropdown-open'),
      $current_dropdown = $('[data-dropdown="'+ $target_link +'"]');

  if($window_width < 1200) {
    // for mobile
    if($this_link.hasClass('active')) {
      $this_link.removeClass('active');
      $current_dropdown.removeClass('active');
    } else {
      $('.main-header__action-link, .main-header__dropdown').removeClass('active');
      $this_link.addClass('active');
      $current_dropdown.addClass('active');
    }

  } else {
    // for desktops
    if($this_link.hasClass('active')) {
      $this_link.removeClass('active');
      $this_dropdown.removeClass('active');
    } else {
      $('.main-header__action-link, .main-header__dropdown').removeClass('active');
      $this_link.addClass('active');
      $this_dropdown.addClass('active');
    }
  }

  // mobile dropdowns container
  if($('.mobile-dropdowns').length) {
    if($('.mobile-dropdowns').find('.active').length) {
      $('.mobile-dropdowns').addClass('active');
    } else {
      $('.mobile-dropdowns').removeClass('active');
    }
  }
  
});

// 
// subnav [dropdown]
// 
if($('.subnav-button').length) {
  $('.subnav-button').on('click', function(){
    $(this)
      .closest('.main-header__dropdown-sidebar')
      .find('.subnav')
      .toggleClass('active');
    $(this)
      .closest('.main-header__dropdown-sidebar')
      .find('.main-header__dropdown-sidebar-list')
      .toggleClass('translate hide');
  });
}


// 
// radio custom
// 
if($('.radio-custom').length) {
  // default
  $('.radio-custom').on('click', '.radio-custom__option', function(){
    var $this = $(this),
        $this_value = $this.data('value'),
        $radio_custom = $('.radio-custom'),
        $radio_option = $('.radio-custom__option'),
        $active_class = 'checked',
        $output_class = 'output',
        $output = $this.closest($radio_custom).find('.' + $output_class);

    // if click no link 
    if(!$(event.target).closest('a').length) {
      if(!$this.closest($radio_custom).hasClass('radio-custom--alt')) {
        // visual
        $this
          .closest($radio_custom)
          .find($radio_option)
          .removeClass($active_class);
        $this
          .addClass($active_class);

        // functionality
        $output.val($this_value);
      }
    }
  });
  // alt
  $('.radio-custom--alt').on('click', '.radio-custom__options', function(){
    var $this = $(this),
        $unchecked = $this.find('.radio-custom__option'),
        $active_class = 'checked',
        $switch_class = 'switch',
        $output_class = 'output',
        $output = $this.closest('.radio-custom--alt').find('.' + $output_class);

    // visual
    $this.toggleClass($switch_class);
    $unchecked.toggleClass($active_class);

    // functionality
    var $this_value = $this.find('.radio-custom__option.checked').data('value');
    $output.val($this_value);
  });
};

// 
// checkbox custom
// 
if($('.checkbox-custom').length) {
  $('.checkbox-custom').on('click', '[data-value]', function(event){
    var $this = $(this),
        $this_value = $this.data('value'),
        $this_group = $this.data('group'),
        $checkbox_custom = $('.checkbox-custom'),
        $checkbox_option = $('.checkbox-custom__option'),
        $active_class = 'checked',
        $output_class = 'output', 
        $output = $this.closest($checkbox_custom).find('.' + $output_class),
        $adv_options = $this.closest($checkbox_custom).find('.checkbox-custom__adv-options[data-group="' + $this_group + '"]').find($checkbox_option);

      // if click no link 
      if(!$(event.target).closest('a').length) {
        // general
        $this
          .toggleClass($active_class);
       
        // group (if has group attr)
        if(typeof $this_group !== typeof undefined && $this_group !== false) {
          // functionality
          $adv_options.each(function(){
            var $all_values = $(this).data('value');
            if($this.hasClass($active_class)) {
              $(this).addClass($active_class);
              $output.find('input[value="' + $all_values + '"]').remove();
              $output.append('<input type="hidden" value="' + $all_values + '">');
            } else {
              $(this).removeClass($active_class);
              $output.find('input[value="' + $all_values + '"]').remove();
            }
          });
        } else {
          // single (if !has group attr)
          // active group items length (if active items == 0 - remove checked class in main group element)
          var $active_group_items = $this.closest('[data-group]').find('.checkbox-custom__option.checked');

          if($active_group_items.length == 0) {
            var $current_group = $this.closest('[data-group]').data('group');
            $this
              .closest($checkbox_custom)
              .find('.checkbox-custom__option[data-group="' + $current_group + '"]')
              .removeClass($active_class);
          }

          // functionality
          // if this elements has active class
          if($this.hasClass($active_class)) {
            // checked main group item when items checked > 0 elements
            if($this.closest('[data-group]').length) {
              var $group = $this.closest('[data-group]').data('group');
              $this
                .closest($checkbox_custom)
                .find('[data-group="' + $group + '"]')
                .addClass($active_class);
            }

            $output.append('<input type="hidden" value="' + $this_value + '">');
          } else {
            $output.find('input[value="' + $this_value + '"]').remove();
          }
        }
      }
  });
}

// 
// select custom
// 
if($('.select-custom').length) {
  // show/hide select
  $('.select-custom').on('click', '.select-custom__placeholder', function(){
    var $this = $(this),
        $this_parent = $this.closest('.select-custom');
    
    if(!$this_parent.hasClass('active')) {
      $('.select-custom').removeClass('active');
      $this_parent.addClass('active');
    } else {
      $this_parent.removeClass('active');
    }
  });
  // select option
  $('.select-custom__dropdown').on('click', '[data-value]', function(){
    var $this = $(this),
        $this_value = $this.data('value'),
        $this_text = $this.text(),
        $select_custom = $this.closest('.select-custom'),
        $select_custom_multi = $this.closest('.select-custom--multi'),
        $data_value_el = $select_custom.find('[data-value]'),
        $placeholder = $select_custom.find('.select-custom__placeholder'),
        $placeholder_default_text = $placeholder.find('.select-custom__placeholder-default-text'),
        $dropdown = $this.closest('.select-custom__dropdown'),
        $output = $select_custom.find('input.output'),
        $output_multi = $select_custom_multi.find('.output');
    
    if($select_custom_multi.length) {
      // multi

      $this.toggleClass('active');
      // functionality
      // if this elements has active class
      if($this.hasClass('active')) {
        $output_multi.append('<input type="hidden" value="' + $this_value + '">');

        $placeholder_default_text.css('display', 'none');

        $placeholder.find('.select-custom__placeholder-text').append('<span data-value="' + $this_value + '">' + $this_text + '</span>');
      } else {
        $output_multi.find('input[value="' + $this_value + '"]').remove();

        $placeholder.find('.select-custom__placeholder-text span[data-value="' + $this_value +'"]').remove();

        if($placeholder_default_text.siblings('span').length == 0) {
          $placeholder_default_text.css('display', 'inline-block');
        }
      }
    } else {
      // single
        // visual
      $select_custom.toggleClass('active');
      $data_value_el.removeClass('active');
      $this.addClass('active');
        // functionality
      $output.val($this_value);
      $placeholder.find('.select-custom__placeholder-text').text($this_text);
    }
  });
  // select live search
  if($('.select-live-search').length) {
    $('.select-live-search').keyup(function(){
        // Retrieve the input field text
        var $this = $(this),
            $filter = $this.val(),
            $count = 0,
            $select_dropdown = $this.closest('.select-custom__dropdown'),
            $live_search_list = $select_dropdown.find('.select-custom__dropdown-list'),
            $live_search_list_item = $live_search_list.find('.select-custom__dropdown-option'),
            $live_search_list_group = $live_search_list.find('.select-custom__dropdown-group'),
            $data_error_text = $live_search_list.data('error-text'),
            $nothing_found_class = 'nothing-found';

        // Loop through the comment list
        $live_search_list_item.each(function(){
          if ($(this).text().search(new RegExp($filter, "i")) < 0) {
            // If the list item does not contain the text phrase fade it out

            if(!$(this).hasClass($nothing_found_class) && !$(this).hasClass('active')) {
              // if no error message or active item - this add class hidden
              $(this).addClass('hidden');
            } 

          } else {
            // Show the list item if the phrase matches
            $(this).removeClass('hidden');
            $count++;
          }

          // click order-last items on search
          $select_dropdown.on('click', '.order-last', function() {
            $(this).addClass('hidden').removeClass('order-last');
          });

          // active items to last order
          if($filter.length > 0) {
            if($(this).hasClass('active')) {
              $(this).addClass('order-last');
            }
          } else {
            if($(this).hasClass('active')) {
              $(this).removeClass('order-last');
            }
          }
        });

        // group items (hidden or show group name)
        $live_search_list_group.each(function(){
          var $summary_items = $(this).siblings('ul').find('.select-custom__dropdown-option'),
              $hidden_items = $(this).siblings('ul').find('.select-custom__dropdown-option.hidden');

          if($summary_items.length == $hidden_items.length) {
            $(this).addClass('hidden');
          } else {
            $(this).removeClass('hidden');
          }
        });

        // if nothing found == show error message
        if($count == 0) {
          if(!$live_search_list.find('.' + $nothing_found_class).length) {
            $live_search_list.append('<li class="select-custom__dropdown-option ' + $nothing_found_class + '">' + $data_error_text + '</li>');
          }
        } else {
          $live_search_list.find('.' + $nothing_found_class).remove();
        }
    })
  }
}

// 
// tabs custom
// 
if($('.tabs-custom').length) {
  if($('.tabs-content--absolute').length) {
    // tabs content height auto
    $(window).on('load resize', function(){
      var $max_height_el = 0;
       $(".tabs-content--absolute .tabs-content__item").each(function () {
           var $height_elements = $(this).outerHeight();
           if($height_elements > $max_height_el) {
              $max_height_el = $height_elements;
           };
       });
       $(".tabs-content--absolute").css('height', $max_height_el);
    }); 
  };
  // tabs nav click
  $('.tabs-custom').on('click', '.tabs-nav__item', function() {
    var $this = $(this),
        $this_data_tab = $(this).data('tab'),
        $current_tab_content = $('.tabs-content__item[data-tab="' + $this_data_tab + '"]'),
        $tabs_parent = $('.tabs-custom'),
        $tabs_nav = $('.tabs-nav'),
        $tabs_content = $('.tabs-content'),
        $active_class = 'active';
        // remove all active classes
        $this
          .closest($tabs_parent)
          .find('.' + $active_class)
          .removeClass($active_class);
        $this
          .closest($tabs_parent)
          .find($tabs_content)
          .find('.' + $active_class)
          .removeClass($active_class);
        // add active class to current element
        $this.addClass($active_class);
        $current_tab_content.addClass($active_class);
  });
}

//
// accordion custom
//
function close_accordion_section() {
    $('.accordion-custom .accordion-link').removeClass('active');
    $('.accordion-custom .accordion-content').slideUp(300);
}

$('.accordion-link').click(function(e) {

    var $current_accordion = $(this).data('accordion');

    if($(this).hasClass('active')) {
      close_accordion_section();
    }else {
      close_accordion_section();
      $(this).addClass('active');
      $('.accordion-content[data-accordion="' + $current_accordion + '"]').slideDown(300); 
    } 
});

// 
// live search
// 
if($('.live-search').length) {
  var $live_search_list = $('.live-search-list'),
      $live_search_list_item = $live_search_list.find('.live-search-list__item'),
      $data_error_text = $live_search_list.data('error-text'),
      $nothing_found_class = 'nothing-found';

  $('.live-search').keyup(function(){
      // Retrieve the input field text
      var $filter = $(this).val(),
          $count = 0;

      // show list only write min. 1 letter 
      if($filter.length > 0) {
        $live_search_list.addClass('show');
      } else {
        $live_search_list.removeClass('show');
      }

      // Loop through the comment list
      $live_search_list_item.each(function(){
        // If the list item does not contain the text phrase fade it out
        if ($(this).text().search(new RegExp($filter, "i")) < 0) {
            $(this).removeClass('show');
        // Show the list item if the phrase matches
        } else {
           $(this).addClass('show');
           $count++;
        }
      });

      // if nothing found
      if($count == 0) {
        if(!$live_search_list.find('.' + $nothing_found_class).length) {
          $live_search_list.append('<li class="live-search-list__item ' + $nothing_found_class + '">' + $data_error_text + '</li>');
        }
      } else {
        $live_search_list.find('.' + $nothing_found_class).remove();
      }
  })
    .on('blur', function(){
      $live_search_list.removeClass('show');
    })
    .on('focus', function(){
      if($(this).val().length > 0) {
        $live_search_list.addClass('show');
      }
  });
}

//
// lightbox
//
if($.fn.magnificPopup) {
  $('.image-container').magnificPopup({
    delegate: 'a',
    type: 'image',
    gallery: {
      enabled: true,
      arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir% icon-arrow_lightbox"></button>'
    },
    closeMarkup: '<i title="%title%" class="mfp-close icon-cross"></i>',
    zoom: {
      enabled: false,
    },
    mainClass: 'mfp-fade mfp-image',
    fixedContentPos: true,
    callbacks: {
      beforeOpen: function() {
       if(!device.android()) {
           document.ontouchmove = function(e){ e.preventDefault()};
       } else {
           $('html').css('overflow', 'hidden');
       }
      },
      close: function() {
        if(!device.android()) {
           document.ontouchmove = function(e){ e.stopPropagation()};
        } else {
           $('html').css('overflow', 'auto');
        }
      }
    }
  });

//
// pop-ups
//
  $('.open-popup-link').magnificPopup({
    type:'inline',
    removalDelay: 500,
    fixedContentPos: true,
    callbacks: {
      beforeOpen: function() {
        this.st.mainClass = this.st.el.attr('data-effect');
        if(!device.android()) {
            document.ontouchmove = function(e){ e.preventDefault()};
            // $('.popup').ontouchmove = function(e){ e.stopPropagation()};
        } else {
            $('html').css('overflow', 'hidden');
        }
      },
      close: function() {
        if(!device.android()) {
           document.ontouchmove = function(e){ e.stopPropagation()};
        } else {
           $('html').css('overflow', 'auto');
        }
      }
    },
    closeMarkup: '<i title="%title%" class="mfp-close icon-cross"></i>'
  });
}

// 
// alert alt
//
if($('.alert-alt-container').length) {
  // position
  $(window).on('load resize scroll', function(){
    var $alert_right_position = $('.container').offset().left,
        $alert_top_position = $('.main-header').outerHeight();
    $('.alert-alt-container').css({
        right: $alert_right_position + 15,
        top: $alert_top_position + 30,
      });
  });

  $('.alert--alt').on('click', '.alert--alt__close', function(){
    var $this = $(this),
        $this_parent = $this.closest('.alert--alt');
    
    $this_parent.addClass('hide');
    setTimeout(function(){
      $this_parent.remove();
    }, 300);
  });
  $('body').on('click', '.alert--alt-show', function(){
    var $this = $(this),
        $this_parent = $this.closest('.alert--alt');
    
    $this_parent.addClass('hide');
    setTimeout(function(){
      $this_parent.remove();
    }, 300);
  });
}


// 
// Carousel
// 
if($('.carousel').length) {
  var $carousel = $('.carousel'),
      $carousel_item = $('.carousel').find('.carousel__item'),
      $carousel_item_class = 'carousel__item';

  $carousel.flickity({
    cellSelector: '.' + $carousel_item_class,
    cellAlign: 'left',
    pageDots: false,
    contain: true,
    prevNextButtons: false,
    accessibility: true,
    wrapAround: false
  });
  // prev/next buttons
  $('.carousel').on( 'click', '.previous-arrow', function() {
    $(this).closest('.carousel').flickity('previous');
  });
  $('.carousel').on( 'click', '.next-arrow', function() {
    $(this).closest('.carousel').flickity('next');
  });

  // hidden arrows if realy q-ty of items less data-items number
  $carousel.each(function(){
    var $carousel_data_item = $(this).data('items'),
        $carousel_realy_items = $(this).find('.' + $carousel_item_class);
    
    if($carousel_realy_items.length <= $carousel_data_item) {
      $(this).find('.carousel__arrows').css('display', 'none');
    } else {
      $(this).find('.carousel__arrows').css('display', 'block');
    }
  });
}

//
// Profile hover actions
// 
if($('.profile-item').length) {
  $('.profile-item').find('.profile-item__hover-actions [data-action]').hover(function(){
    var $this = $(this),
        $this_data_action = $this.data('action'),
        $this_parent = $this.closest('.profile-item__hover'),
        $this_placeholder_data = $this_parent.find('.profile-item__hover-placeholder [data-action="' + $this_data_action + '"]');

      // only desktop
      $this_parent.toggleClass('active');
      $this_placeholder_data.toggleClass('active');
  });
}

// 
// homepage quick search position
// 
// if($('.homepage-section--quick-search-form').length) {
//   $(window).on('load resize', function(){
//     var $form = $('.homepage-section--quick-search-form'),
//         $container = $('.main-header > .container'),
//         $container_offset = $container.offset().left;
    
//     $form.css('margin-left', ($container_offset - 15) + 'px');
//         console.log($container_offset);

//   });
// }
//
// on body click
//
$('body').on('click touchstart', function(event){
  // main header and main mobile dropdowns
  if(!$(event.target).closest('.main-header').length) {
    if(!$(event.target).closest('.mobile-dropdowns').length) {
      if(!$(event.target).closest('.mfp-wrap').length) {
        $('.main-header__nav, .menu-trigger, .main-header__dropdown, .main-header__action-link').removeClass('active');
      }
    }
  }
  // selects
  if(!$(event.target).closest('.select-custom').length) {
    $('.select-custom').removeClass('active');
  }
});

//
// browser navigator
//
var $browser_name = navigator.userAgent;
if ($browser_name.toLowerCase().indexOf('edge') > -1) {
  $('html').addClass('edge').removeClass('chrome');
}
if ($browser_name.toLowerCase().indexOf('trident') > -1) {
  $('html').addClass('ie ie-11');
}
if ($browser_name.toLowerCase().indexOf('msie 10.0') > -1) {
  $('html').addClass('ie-10').removeClass('ie-11');
}
if ($browser_name.toLowerCase().indexOf('msie 9.0') > -1) {
  $('html').addClass('ie-9').removeClass('ie-11');
};
//
// validate form
//
if($('.validate').length) {
  $.validator.setDefaults({
      errorPlacement: function() {}
  });
  $('form.validate').each(function() {
      $(this).validate();
  });
} 

});
